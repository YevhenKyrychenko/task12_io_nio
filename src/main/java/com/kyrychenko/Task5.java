package com.kyrychenko;

import java.io.*;

public class Task5 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("D:\\epam\\task12_io_nio\\src\\main\\java\\com\\kyrychenko\\Droid.java"));
        String line = br.readLine();

        while (line != null) {
            String str = line.trim();
            if (str.startsWith("//") || str.startsWith("/**") || str.startsWith("*") || str.startsWith("*/")){
                System.out.println(line);
            }
            line = br.readLine();
        }
    }
}
