package com.kyrychenko;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class Task6 {
    private static Logger logger = LogManager.getLogger(Task6.class);
    public static void main(String[] args) throws IOException {
        File f = new File(".");

        File[] files = f.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                logger.info("directory:");
            } else {
                logger.info("     file:");
            }
            logger.info(file.getCanonicalPath());
        }
    }
}
