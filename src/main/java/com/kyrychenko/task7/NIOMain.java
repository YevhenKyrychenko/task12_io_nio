package com.kyrychenko.task7;

import com.kyrychenko.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.RandomAccessFile;

public class NIOMain {
    private static Logger logger = LogManager.getLogger(NIOMain.class);

    public static void main(String[] args) throws IOException {
        RandomAccessFile raf = new RandomAccessFile("text.txt","rw");
        StringNIOBuffer buf = new StringNIOBuffer(raf.getChannel());

        logger.info(buf.read());

        RandomAccessFile raf1 = new RandomAccessFile("output.txt","rw");
        StringNIOBuffer buf1 = new StringNIOBuffer(raf1.getChannel());
        buf1.write("I can write here!");
        buf1.close();
        buf.close();
    }
}
